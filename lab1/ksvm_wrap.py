import matplotlib.pyplot as plt
import numpy as np
from sklearn.svm import SVC

from lab1 import data
from lab1.data import eval_perf_multi
from lab1.util import one_hot_encode, graph_result


class KSVMWrap:
    def __init__(self, X, Y_, param_svm_c=1.0, param_svm_gamma='auto'):
        self.clf = SVC(C=param_svm_c, gamma=param_svm_gamma)
        self.clf.fit(X, Y_)

    def predict(self, X):
        return self.clf.predict(X)

    def get_scores(self, X):
        return self.clf.decision_function(X)

    def support(self):
        return self.clf.support_


if __name__ == '__main__':
    np.random.seed(100)

    X, Y_ = data.sample_gmm_2d(6, 2, 10)
    Yoh_ = one_hot_encode(Y_)

    ksvm = KSVMWrap(X, Y_)
    a, pr, _ = eval_perf_multi(ksvm.predict(X), Y_)
    print("Accuracy: {}".format(a))
    print("Precision: {}".format(pr[0]))
    print("Recall: {}".format(pr[1]))
    graph_result(ksvm.get_scores, X, Y_, ksvm.predict(X), ksvm.support(), offset=0.0)
    plt.show()
