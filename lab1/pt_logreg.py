import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import matplotlib.pyplot as plt

from lab1 import data


class PTLogreg(nn.Module):
    def __init__(self, D, C):
        """Arguments:
           - D: dimensions of each datapoint
           - C: number of classes
        """
        super().__init__()

        # parameter initialization
        self.W = nn.Parameter(torch.randn((D, C), dtype=torch.double), requires_grad=True)
        self.b = nn.Parameter(torch.zeros(C), requires_grad=True)

    def forward(self, X):
        return torch.softmax(X.mm(self.W) + self.b, dim=1)

    def get_loss(self, X, Yoh_, param_lambda):
        loss = -torch.log((self.forward(X) * Yoh_).sum(dim=1)).mean()
        if param_lambda != 0.0:
            loss += self.W.norm() * param_lambda
        return loss


def train(model, X, Yoh_, param_niter, param_delta, param_lambda=0.0):
    """Arguments:
       - X: model inputs [NxD], type: torch.Tensor
       - Yoh_: ground truth [NxC], type: torch.Tensor
       - param_niter: number of training iterations
       - param_delta: learning rate
    """
    # optimizer initialization
    optimizer = optim.SGD(model.parameters(), lr=param_delta)

    # training loop
    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_, param_lambda)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        # diagnostic output
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))


def eval(model, X):
    """Arguments:
       - model: type: PTLogreg
       - X: actual datapoints [NxD], type: np.array
       Returns: predicted class probabilites [NxC], type: np.array
    """
    return model.forward(torch.tensor(X)).detach().numpy()


def one_hot_encode(Y_):
    N, C = Y_.shape[0], np.max(Y_) + 1
    Yoh_ = np.zeros((N, C))
    Yoh_[np.arange(N), Y_] = 1
    return Yoh_


def logreg_decfun(model):
    def classify(X):
        return np.argmax(eval(model, X), axis=1)

    return classify


if __name__ == "__main__":
    # initialize random number generators
    np.random.seed(100)
    torch.random.manual_seed(100)

    # instantiate the dataset
    X, Y_ = data.sample_gauss_2d(3, 100)
    Yoh_ = one_hot_encode(Y_)

    # model definition
    ptlr: PTLogreg = PTLogreg(X.shape[1], Yoh_.shape[1])

    # train the parameters
    train(ptlr, torch.tensor(X), torch.tensor(Yoh_), 1000, 0.5)
    # train(ptlr, torch.tensor(X), torch.tensor(Yoh_), 1000, 0.5, 1e-3)  # regularization
    # train(ptlr, torch.tensor(X), torch.tensor(Yoh_), 1000, 5e-5)  # small param_delta

    # training dataset probabilities
    probs = eval(ptlr, X)
    Y = np.argmax(probs, axis=1)

    # report performance
    accuracy, pr, M = data.eval_perf_multi(Y, Y_)
    print("Precision and recall (by class):")
    print(pr)

    # graph the results
    decfun = logreg_decfun(ptlr)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)
    data.graph_data(X, Y_, Y)
    plt.show()
