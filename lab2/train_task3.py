import time
from pathlib import Path

import nn_task3
import numpy as np
import torch
from convolutional_model import ConvolutionalModel
from torch import nn
from torchvision.datasets import MNIST

DATA_DIR = Path(__file__).parent / 'datasets' / 'MNIST'
SAVE_DIR = Path(__file__).parent / 'out'

config = {}
config['max_epochs'] = 8
config['batch_size'] = 50
config['save_dir'] = SAVE_DIR
config['weight_decay'] = 1e-2
config['lr_policy'] = {1: 1e-1, 3: 1e-2, 5: 1e-3, 7: 1e-4}

# np.random.seed(100)
np.random.seed(int(time.time() * 1e6) % 2 ** 31)

ds_train, ds_test = MNIST(DATA_DIR, train=True, download=True), MNIST(DATA_DIR, train=False)
train_x = ds_train.data.reshape([-1, 1, 28, 28]).numpy().astype(np.float) / 255
train_y = ds_train.targets.numpy()
train_x, valid_x = train_x[:55000], train_x[55000:]
train_y, valid_y = train_y[:55000], train_y[55000:]
test_x = ds_test.data.reshape([-1, 1, 28, 28]).numpy().astype(np.float) / 255
test_y = ds_test.targets.numpy()
train_mean = train_x.mean()
train_x, valid_x, test_x = (x - train_mean for x in (train_x, valid_x, test_x))

train_x = torch.from_numpy(train_x).float()
train_y = torch.from_numpy(train_y)
valid_x = torch.from_numpy(valid_x).float()
valid_y = torch.from_numpy(valid_y)

inputs = np.random.randn(config['batch_size'], 1, 28, 28)
model = ConvolutionalModel(inputs,
                           conv1_width=16, conv2_width=32,
                           fc1_width=512, class_count=10)

nn_task3.train(train_x, train_y, valid_x, valid_y, model, config)
nn_task3.evaluate("Test", torch.from_numpy(test_x).float(), torch.from_numpy(test_y), model, nn.CrossEntropyLoss(),
                  config)
