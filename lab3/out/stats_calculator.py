import numpy as np
from numpy import nan
from copy import deepcopy

path = 'hyperparameters_5_compare_hyperparameters.txt'
# path = 'hyperparameters_5_compare_hyperparameters_baseline.txt'
# path = 'hyperparameters_5_compare_hyperparameters_lstm.txt'
# path = 'hyperparameters_2_compare_cells_complex.txt'

with open(path, 'r') as f:
    results = list(map(eval, f.read().splitlines()))


def extract_metric(result):
    metric_index = 0  # accuracy
    result['metrics'] = result['metrics'][metric_index]
    return result


results = list(map(extract_metric, results))
metrics = list(map(lambda r: r['metrics'], results))
params = deepcopy(results)
for result in params:
    result.pop('metrics', None)

keys = params[0].keys()

possible_values = dict()
for key in keys:
    possible_values[key] = set()
for result in results:
    for key in keys:
        possible_values[key].add(result[key])
for key in keys:
    possible_values[key] = sorted(list(possible_values[key]))

print("Parameters:")
for key in filter(lambda k: k != 'metrics', keys):
    print("=> {} - chosen from {}".format(key, possible_values[key]))
print()

print("Absolute best: {}\n=> params: {}\n".format(np.max(metrics), params[int(np.argmax(metrics))]))
print("Absolute worst: {}\n=> params: {}".format(np.min(metrics), params[int(np.argmin(metrics))]))

for key in keys:
    print("\nParameter {}:".format(key))
    for possible_value in possible_values[key]:
        filtered = list(map(lambda r: r['metrics'], filter(lambda r: r[key] == possible_value, results)))
        print(" {} - mean = {}, stddev = {}".format(possible_value, np.mean(filtered), np.std(filtered)))
