import math
import os

import matplotlib.pyplot as plt
import numpy as np
import skimage as ski
import skimage.io
import torch
import torch.nn as nn
import torch.optim as optim


def draw_conv_filters(epoch, step, weights, save_dir, name):
    w = weights.copy()
    num_filters = w.shape[0]
    num_channels = w.shape[1]
    k = w.shape[2]
    assert w.shape[2] == w.shape[3]

    w -= w.min()
    w /= w.max()

    border = 1
    cols = 8
    rows = math.ceil(num_filters / cols)
    width = cols * k + (cols - 1) * border
    height = rows * k + (rows - 1) * border

    for i in range(1):
        img = np.zeros([height, width])
        for j in range(num_filters):
            r = int(j / cols) * (k + border)
            c = int(j % cols) * (k + border)
            img[r:r + k, c:c + k] = w[j, i]
        filename = '%s_epoch_%02d_step_%06d_input_%03d.png' % (name, epoch, step, i)
        ski.io.imsave(os.path.join(save_dir, filename), img)


def train(train_x, train_y, valid_x, valid_y, model, config, criterion=nn.CrossEntropyLoss()):
    max_epochs = config['max_epochs']
    batch_size = config['batch_size']
    save_dir = config['save_dir']
    weight_decay = config['weight_decay']
    lr_policy = config['lr_policy']

    loss_values = []

    num_examples = train_x.shape[0]
    assert num_examples % batch_size == 0
    num_batches = num_examples // batch_size

    lr = lr_policy[1]

    for epoch in range(1, max_epochs + 1):
        if epoch in lr_policy:
            lr = lr_policy[epoch]

        optimizer = optim.SGD(model.parameters(), lr, weight_decay=weight_decay)

        cnt_correct = 0.0
        permutation_idx = np.random.permutation(num_examples)
        train_x = train_x[permutation_idx]
        train_y = train_y[permutation_idx]

        for i in range(num_batches):
            batch_x = train_x[i * batch_size:(i + 1) * batch_size, :]
            batch_y = train_y[i * batch_size:(i + 1) * batch_size].long()

            logits = model.forward(batch_x)
            loss = criterion(logits, batch_y)
            loss_values.append(loss.detach())

            yp = torch.argmax(logits, 1)
            cnt_correct += (yp == batch_y).sum().detach()

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            if i % 5 == 0:
                print("epoch %d, step %d/%d, batch loss = %.2f" % (epoch, i * batch_size, num_examples, loss.item()))
            if i % 100 == 0:
                draw_conv_filters(epoch, i * batch_size, model.conv1.weight.detach().numpy(), save_dir, "conv1")
            if i > 0 and i % 50 == 0:
                print("Train accuracy = %.2f" % (cnt_correct / ((i + 1) * batch_size) * 100))
        print("Train accuracy = %.2f" % (cnt_correct / num_examples * 100))
        evaluate("Validation", valid_x, valid_y, model, criterion, config)

    loss_plot_path = os.path.join(save_dir, 'loss_plot.png')
    print('Plotting in: ', loss_plot_path)
    plt.plot(loss_values)
    plt.savefig(loss_plot_path)


def evaluate(name, x, y, model, criterion, config):
    print("\nRunning evaluation:", name)

    batch_size = config['batch_size']
    num_examples = x.shape[0]
    assert num_examples % batch_size == 0
    num_batches = num_examples // batch_size

    cnt_correct = 0.0
    loss_avg = 0

    for i in range(num_batches):
        batch_x = x[i * batch_size:(i + 1) * batch_size, :]
        batch_y = y[i * batch_size:(i + 1) * batch_size]

        logits = model.forward(batch_x)

        yp = torch.argmax(logits, 1)
        cnt_correct += (yp == batch_y).sum().detach()

        loss_val = criterion(logits, batch_y).detach()
        loss_avg += loss_val

    valid_acc = cnt_correct / num_examples * 100
    loss_avg /= num_batches

    print(name + " accuracy = %.2f" % valid_acc)
    print(name + " avg loss = %.2f\n" % loss_avg)
