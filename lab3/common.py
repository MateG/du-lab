import numpy as np
import torch
from attr import dataclass
from nlp_dataset import *
from torch import nn
from torch import optim
from torch.utils import data


@dataclass
class Args:
    seed: int = 0
    epochs: int = 5

    train_batch_size: int = 10
    valid_batch_size: int = 32
    test_batch_size: int = 32

    lr: float = 1e-4
    gradient_clip: float = 0.25

    text_max_size: int = -1
    text_min_freq: int = 0

    vectors_size: int = 300
    vectors_path: str = 'dataset/sst_glove_6b_300d.txt'

    pretrained_freeze: bool = True

    fc_transfer: any = torch.relu


def common_main(datasets, args, model_creator):
    reset_seed(args.seed)

    train_dataset, valid_dataset, test_dataset = datasets

    text_vocab = Vocab(train_dataset.text_frequencies(), args.text_max_size, args.text_min_freq, True)
    label_vocab = Vocab(train_dataset.label_frequencies(), -1, 0, False)
    set_vocabs(datasets, text_vocab, label_vocab)

    dataloaders = (
        data.DataLoader(train_dataset, args.train_batch_size, shuffle=True, collate_fn=pad_collate_fn),
        data.DataLoader(valid_dataset, args.valid_batch_size, collate_fn=pad_collate_fn),
        data.DataLoader(test_dataset, args.test_batch_size, collate_fn=pad_collate_fn)
    )

    embedding_values = embedding_matrix(text_vocab.stoi, args.vectors_size, args.vectors_path)
    embedding = nn.Embedding.from_pretrained(embedding_values, freeze=args.pretrained_freeze, padding_idx=0)

    model = model_creator(embedding)

    criterion = nn.BCEWithLogitsLoss()
    optimizer = optim.Adam(model.parameters(), lr=args.lr)

    return train_and_evaluate(model, dataloaders, optimizer, criterion, args.epochs, args.gradient_clip)


def reset_seed(seed):
    np.random.seed(seed)
    torch.manual_seed(seed)


def load_datasets(train_path, valid_path, test_path):
    train_dataset = NLPDataset.from_file(train_path)
    valid_dataset = NLPDataset.from_file(valid_path)
    test_dataset = NLPDataset.from_file(test_path)

    return train_dataset, valid_dataset, test_dataset


def set_vocabs(datasets, text_vocab, label_vocab):
    for dataset in datasets:
        dataset.text_vocab = text_vocab
        dataset.label_vocab = label_vocab


def train_and_evaluate(model, dataloaders, optimizer, criterion, epochs, gradient_clip):
    train_dataloader, valid_dataloader, test_dataloader = dataloaders

    for epoch in range(epochs):
        print("Training epoch {}/{}...".format(epoch + 1, epochs))
        train(model, train_dataloader, optimizer, criterion, gradient_clip)

        # print()

        # print("Evaluating on validation set...")
        evaluate(model, valid_dataloader, criterion)

        # print()

    # print("Evaluating on test set...")
    return evaluate(model, test_dataloader, criterion)


def train(model, loader, optimizer, criterion, gradient_clip):
    model.train()

    epoch_loss = 0.0

    for batch_num, batch in enumerate(loader):
        optimizer.zero_grad()

        x, y, lengths = batch
        logits = model((x, lengths))

        loss = criterion(logits.view(logits.shape[0]), y.float())
        epoch_loss += loss.item()

        loss.backward()
        nn.utils.clip_grad_norm_(model.parameters(), gradient_clip)
        optimizer.step()

    # print("Loss = {}".format(epoch_loss))


def evaluate(model, loader, criterion):
    model.eval()

    total_loss = 0.0
    yp = None
    yt = None

    with torch.no_grad():
        for batch_num, batch in enumerate(loader):
            x, y, lengths = batch

            logits = model((x, lengths))
            logits = logits.view(logits.shape[0])

            loss = criterion(logits, y.float())
            total_loss += loss.item()

            if yp is None:
                yp = logits.detach().numpy()
                yt = y.detach().numpy()
            else:
                yp = np.append(yp, logits.detach().numpy(), 0)
                yt = np.append(yt, y.detach().numpy(), 0)

    # print("Loss = {}".format(total_loss))

    confusion, acc, pr, rec, f1 = calculate_metrics(yp, yt)
    # print("Confusion matrix:")
    # print(confusion)
    # print("Accuracy  = {}".format(acc))
    # print("Precision = {}".format(pr))
    # print("Recall    = {}".format(rec))
    # print("F1        = {}".format(f1))

    return acc, pr, rec, f1


def calculate_metrics(yp, yt):
    yp = np.where(yp > 0.0, 1, 0)

    confusion = np.empty((2, 2), dtype=int)
    for ct in range(2):
        ct_indices = np.argwhere(yt == ct)
        for cp in range(2):
            confusion[ct][cp] = (yp[ct_indices] == cp).sum()

    tp = confusion[0][0]
    fn = confusion[0][1]
    fp = confusion[1][0]
    tn = confusion[1][1]

    accuracy = (tp + tn) / yp.shape[0]
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f1 = 2 * recall * precision / (recall + precision)

    return confusion, accuracy, precision, recall, f1
