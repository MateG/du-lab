import torch
from common import common_main, load_datasets, Args
from torch import nn


class BaselineModel(nn.Module):

    def __init__(self, embedding, fc_transfer):
        super().__init__()

        self.embedding = embedding

        self.fc_transfer = fc_transfer

        self.fc1 = nn.Linear(300, 150)
        self.fc2 = nn.Linear(150, 150)
        self.fc3 = nn.Linear(150, 1)

    def forward(self, x):
        h = torch.sum(self.embedding(x[0]), 1) / x[1].reshape(-1, 1)

        h = self.fc1(h)
        h = self.fc_transfer(h)

        h = self.fc2(h)
        h = self.fc_transfer(h)

        h = self.fc3(h)
        return h


def main(datasets, args):
    return common_main(datasets, args, lambda embedding: BaselineModel(embedding, args.fc_transfer))


if __name__ == '__main__':
    datasets = load_datasets(
        'dataset/sst_train_raw.csv',
        'dataset/sst_valid_raw.csv',
        'dataset/sst_test_raw.csv'
    )

    main(datasets, Args())
