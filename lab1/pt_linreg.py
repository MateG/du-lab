import torch
import torch.optim as optim


def generate_data(x_min, x_max):
    X = [*range(x_min, x_max + 1)]
    Y = [2 * x + 1 for x in X]
    return X, Y


if __name__ == '__main__':
    # parameters
    a = torch.randn(1, requires_grad=True)
    b = torch.randn(1, requires_grad=True)

    # data
    X_values, Y_values = generate_data(1, 10)
    X = torch.tensor(X_values)
    Y = torch.tensor(Y_values)

    # optimization: gradient descent
    optimizer = optim.SGD([a, b], lr=0.001)

    for i in range(100):
        # affine regression model
        Y_ = a * X + b

        diff = (Y - Y_)

        # quadratic loss
        loss = torch.mean(diff * diff)

        # gradient calculation
        loss.backward()

        print('step: {}, loss: {}'.format(i, loss))
        print('\tY_: {}'.format(Y_))
        print('\ta.grad: {}\n\tb.grad: {}'.format(a.grad, b.grad))

        # calculated analytically
        a_grad = (diff * -2 * X).sum() / len(X)
        b_grad = (diff * -2).sum() / len(X)

        print('\ta_grad calculated: {}'.format(a_grad))
        print('\tb_grad calculated: {}'.format(b_grad))

        # optimization step
        optimizer.step()

        # gradient reset
        optimizer.zero_grad()

        print('\ta: {}\n\tb: {}'.format(a, b))
