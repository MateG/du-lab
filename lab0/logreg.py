import matplotlib.pyplot as plt
import numpy as np

from lab0 import data


def logreg_train(X, Y_, param_delta=0.05, param_niter=50000):
    """
    :param X: data, np.array NxD
    :param Y_: class indexes, np.array Nx1
    :param param_delta: learning rate
    :param param_niter: number of gradient descent iterations
    :return: W, b - logistic regression parameters
    """
    N, D = X.shape
    C = np.max(Y_) + 1

    # initialize results
    W = np.random.randn(D, C)
    b = np.zeros(C)

    # one hot encoded class index matrix
    Y_onehot = np.zeros((N, C))
    Y_onehot[np.arange(N), Y_] = 1

    # gradient descent
    for i in range(param_niter):
        scores = np.dot(X, W) + b  # N x C
        expscores = np.exp(scores)  # N x C
        sumexp = np.sum(expscores, axis=1)  # N x 1
        probs = expscores / sumexp[:, None]  # N x C
        logprobs = np.log(probs)  # N x C
        loss = -logprobs[np.arange(N), Y_].sum()  # scalar

        # diagnostic output
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivative of loss components with respect to scores
        dL_ds = probs - Y_onehot  # N x C

        # gradients
        grad_W = 1.0 / N * np.dot(X.T, dL_ds)  # D x C
        grad_b = 1.0 / N * np.sum(dL_ds, axis=0)  # 1 x C

        # update results
        W += -param_delta * grad_W
        b += -param_delta * grad_b

    return W, b


def logreg_classify(X, W, b):
    expscores = np.exp(np.dot(X, W) + b)
    return np.argmax(expscores / np.sum(expscores), axis=1)


def logreg_decfun(W, b):
    def classify(X):
        return logreg_classify(X, W, b)

    return classify


if __name__ == '__main__':
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gauss_2d(3, 100)

    # train the model
    W, b = logreg_train(X, Y_)

    # graph the decision surface
    decfun = logreg_decfun(W, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, logreg_classify(X, W, b), special=[])

    # show the plot
    plt.show()
