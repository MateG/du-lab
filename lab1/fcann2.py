import matplotlib.pyplot as plt
import numpy as np

from lab1 import data


def fcann2_train(X, Y_, H=5, param_delta=0.05, param_niter=100_000, param_lambda=1e-3):
    N, D = X.shape
    C = np.max(Y_) + 1

    # initialize results
    W1 = np.random.randn(D, H)
    b1 = np.zeros(H)
    W2 = np.random.randn(H, C)
    b2 = np.zeros(C)

    # one hot encoded class index matrix
    Y_oh = np.zeros((N, C), dtype=int)
    Y_oh[np.arange(N), Y_] = 1

    # gradient descent
    for i in range(param_niter):
        S1 = np.dot(X, W1) + b1  # N x H
        H1 = np.maximum(S1, 0)  # N x H
        S2 = np.dot(H1, W2) + b2  # N x C
        expscores = np.exp(S2 - np.max(S2, axis=1).reshape(-1, 1))  # N x C
        sumexp = np.sum(expscores, axis=1).reshape(-1, 1)  # N x 1
        probs = expscores / sumexp  # N x C

        # diagnostic output
        if i % 10 == 0:
            logprobs = np.log(probs[np.arange(N), Y_])  # N x 1
            loss = -logprobs.sum() / N  # scalar
            print("iteration {}: loss {}".format(i, loss))

        G_s2 = (probs - Y_oh) / N  # N x C

        grad_W2 = np.dot(H1.T, G_s2) + param_lambda * W2  # H x C
        grad_b2 = np.sum(G_s2, axis=0)  # 1 x C

        G_h1 = np.dot(G_s2, W2.T)  # N x H
        G_s1 = G_h1  # N x H
        G_s1[H1 <= 0] = 0

        grad_W1 = np.dot(X.T, G_s1) + param_lambda * W1  # D x H
        grad_b1 = np.sum(G_s1, axis=0)  # 1 x H

        # update results
        W1 -= param_delta * grad_W1
        b1 -= param_delta * grad_b1
        W2 -= param_delta * grad_W2
        b2 -= param_delta * grad_b2

    return W1, b1, W2, b2


def fcann2_classify(X, W1, b1, W2, b2):
    H1 = np.dot(X, W1) + b1
    np.maximum(H1, 0, H1)
    S2 = np.dot(H1, W2) + b2
    expscores = np.exp(S2 - np.max(S2, axis=1).reshape(-1, 1))
    return np.argmax(expscores / np.sum(expscores), axis=1)


def fcann2_decfun(W1, b1, W2, b2):
    if len(b2) == 2:
        def classify(X):
            H1 = np.dot(X, W1) + b1
            np.maximum(H1, 0, H1)
            S2 = np.dot(H1, W2) + b2
            expscores = np.exp(S2 - np.max(S2, axis=1).reshape(-1, 1))
            return expscores[:, 0]
    else:
        def classify(X):
            return fcann2_classify(X, W1, b1, W2, b2)

    return classify


if __name__ == '__main__':
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gmm_2d(6, 2, 10)

    # train the model
    W1, b1, W2, b2 = fcann2_train(X, Y_)

    # graph the decision surface
    decfun = fcann2_decfun(W1, b1, W2, b2)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, fcann2_classify(X, W1, b1, W2, b2), special=[])

    # show the plot
    plt.show()
