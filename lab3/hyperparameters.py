import torch
from torch import nn
import numpy as np
import rnn_model
import baseline_model
from common import load_datasets, Args


def compare_cells_simple(datasets):
    results = dict()
    for rnn in [nn.RNN, nn.GRU, nn.LSTM]:
        metrics = np.empty((0, 4), float)
        for seed in range(2):
            s_metrics = rnn_model.main(datasets, Args(seed=seed), rnn)
            metrics = np.append(metrics, np.asarray(s_metrics).reshape(1, -1), 0)
        metrics = np.mean(metrics, axis=0)
        results[rnn.__name__] = metrics
    print(results)


def compare_cells_complex(datasets):
    results = []
    for rnn in [nn.RNN, nn.GRU, nn.LSTM]:
        for hidden_size in [50, 150, 300]:
            for num_layers in [1, 2, 3]:
                for dropout in [0.0, 0.5, 0.9]:
                    if num_layers == 1 and dropout != 0.0:
                        continue
                    for bidirectional in [False, True]:
                        print(
                            'rnn:', rnn.__name__,
                            'hidden_size:', hidden_size,
                            'num_layers:', num_layers,
                            'dropout:', dropout,
                            'bidirectional:', bidirectional
                        )
                        metrics = rnn_model.main(
                            datasets,
                            Args(),
                            rnn,
                            hidden_size,
                            num_layers,
                            dropout,
                            bidirectional
                        )
                        results.append({
                            'rnn': rnn.__name__,
                            'hidden_size': hidden_size,
                            'num_layers': num_layers,
                            'dropout': dropout,
                            'bidirectional': bidirectional,
                            'metrics': metrics
                        })

    for result in results:
        print(result)


def run_best_cell(datasets):
    # Best from compare_cells_complex:
    # {
    #   'rnn': 'LSTM',
    #   'hidden_size': 150,
    #   'num_layers': 3,
    #   'dropout': 0.0,
    #   'bidirectional': True,
    #   'metrics': (0.8084862385321101, 0.828978622327791, 0.786036036036036, 0.8069364161849711)
    # }

    metrics = np.empty((0, 4), float)
    for seed in range(5):
        s_metrics = rnn_model.main(datasets, Args(seed=seed), nn.LSTM, 150, 3, 0.0, True)
        metrics = np.append(metrics, np.asarray(s_metrics).reshape(1, -1), 0)
    print(metrics)

    print("Mean: {}".format(np.mean(metrics, axis=0)))
    print("Standard deviation: {}".format(np.std(metrics, axis=0)))


def compare_vector_prelearning(datasets):
    prelearning_args = Args()
    no_prelearning_args = Args(vectors_path=None, pretrained_freeze=False)

    print("Best RNN:")
    print("With: {}".format(rnn_model.main(datasets, prelearning_args, nn.LSTM, 150, 3, 0.0, True)))
    print("Without: {}".format(rnn_model.main(datasets, no_prelearning_args, nn.LSTM, 150, 3, 0.0, True)))

    print("Baseline:")
    print("With: {}".format(baseline_model.main(datasets, prelearning_args)))
    print("Without: {}".format(baseline_model.main(datasets, no_prelearning_args)))


def compare_hyperparameters(datasets):
    rnn = nn.LSTM
    results = []
    for text_min_freq in [0, 10, 100]:
        for lr in [1e-3, 1e-4, 1e-5]:
            for hidden_size in [50, 150, 300]:
                for fc_transfer in [torch.relu, torch.tanh, torch.sigmoid]:
                    for gradient_clip in [2.5e-8, 2.5e-1]:
                        print(
                            'model:', rnn.__name__,
                            'text_min_freq:', text_min_freq,
                            'lr:', lr,
                            'hidden_size:', hidden_size,
                            'fc_transfer:', fc_transfer.__name__,
                            'gradient_clip:', gradient_clip
                        )
                        args = Args(
                            text_min_freq=text_min_freq,
                            lr=lr,
                            fc_transfer=fc_transfer,
                            gradient_clip=gradient_clip
                        )
                        metrics = rnn_model.main(datasets, args, rnn, hidden_size)
                        print(metrics)
                        results.append({
                            'model': rnn.__name__,
                            'text_min_freq': text_min_freq,
                            'lr': lr,
                            'hidden_size': hidden_size,
                            'fc_transfer': fc_transfer.__name__,
                            'gradient_clip': gradient_clip,
                            'metrics': metrics
                        })

                        if hidden_size == 150:
                            print(
                                'model:', 'Baseline',
                                'text_min_freq:', text_min_freq,
                                'lr:', lr,
                                'hidden_size:', hidden_size,
                                'fc_transfer:', fc_transfer.__name__,
                                'gradient_clip:', gradient_clip
                            )
                            metrics = baseline_model.main(datasets, args)
                            print(metrics)
                            results.append({
                                'model': 'Baseline',
                                'text_min_freq': text_min_freq,
                                'lr': lr,
                                'hidden_size': hidden_size,
                                'fc_transfer': fc_transfer.__name__,
                                'gradient_clip': gradient_clip,
                                'metrics': metrics
                            })
    for result in results:
        print(result)


def run_best_hyperparameters(datasets):
    # Best from compare_hyperparameters:
    # {
    #   'model': 'LSTM',
    #   'text_min_freq': 0,
    #   'lr': 0.0001,
    #   'hidden_size': 150,
    #   'fc_transfer': 'sigmoid',
    #   'gradient_clip': 0.25,
    #   'metrics': (0.8004587155963303, 0.8245192307692307, 0.7725225225225225, 0.797674418604651)
    # }

    args = Args(text_min_freq=0, lr=0.0001, fc_transfer=torch.sigmoid, gradient_clip=0.25)

    metrics = np.empty((0, 4), float)
    for seed in range(5):
        args.seed = seed
        s_metrics = rnn_model.main(datasets, args, nn.LSTM)
        metrics = np.append(metrics, np.asarray(s_metrics).reshape(1, -1), 0)
    print("Mean: {}".format(np.mean(metrics, axis=0)))
    print("Standard deviation: {}".format(np.std(metrics, axis=0)))

    metrics = np.empty((0, 4), float)
    for seed in range(5):
        args.seed = seed
        s_metrics = baseline_model.main(datasets, args)
        metrics = np.append(metrics, np.asarray(s_metrics).reshape(1, -1), 0)
    print("Mean: {}".format(np.mean(metrics, axis=0)))
    print("Standard deviation: {}".format(np.std(metrics, axis=0)))


if __name__ == '__main__':
    datasets = load_datasets(
        'dataset/sst_train_raw.csv',
        'dataset/sst_valid_raw.csv',
        'dataset/sst_test_raw.csv'
    )

    # compare_cells_simple(datasets)
    # compare_cells_complex(datasets)
    # run_best_cell(datasets)
    # compare_vector_prelearning(datasets)
    # compare_hyperparameters(datasets)
    run_best_hyperparameters(datasets)
