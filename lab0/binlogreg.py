import matplotlib.pyplot as plt
import numpy as np

from lab0 import util, data


def binlogreg_train(X, Y_, param_delta=0.01, param_niter=1000):
    """
    :param X: data, np.array NxD
    :param Y_: class indexes, np.array Nx1
    :param param_delta: learning rate
    :param param_niter: number of gradient descent iterations
    :return: w, b - logistic regression parameters
    """
    N, D = X.shape

    # initialize results
    w = np.random.randn(D)
    b = 0.0

    # gradient descent
    for i in range(param_niter):
        scores = np.dot(X, w) + b  # N x 1
        probs = util.sigmoid(scores)  # N x 1
        loss = -np.log(probs).sum()  # scalar

        # diagnostic output
        if i % 10 == 0:
            print("iteration {}: loss {}".format(i, loss))

        # derivative of loss with respect to scores
        dL_dscores = probs - Y_  # N x 1

        # gradients
        grad_w = 1.0 / N * (dL_dscores * X.transpose()).sum()  # D x 1
        grad_b = 1.0 / N * dL_dscores.sum()  # 1 x 1

        # update results
        w += -param_delta * grad_w
        b += -param_delta * grad_b

    return w, b


def binlogreg_classify(X, w, b):
    """
    :param X: data, np.array NxD
    :param w: logistic regression weights
    :param b: logistic regression bias
    :return: probs - c1 class probabilities
    """
    return util.sigmoid(np.dot(X, w) + b)


def binlogreg_decfun(w, b):
    def classify(X):
        return binlogreg_classify(X, w, b)

    return classify


if __name__ == "__main__":
    np.random.seed(100)

    # get the training dataset
    X, Y_ = data.sample_gauss_2d(2, 100)

    # train the model
    w, b = binlogreg_train(X, Y_)

    # evaluate the model on the training dataset
    probs = binlogreg_classify(X, w, b)
    Y = util.step(probs)

    # report performance
    accuracy, recall, precision = data.eval_perf_binary(Y, Y_)
    AP = data.eval_AP(Y_[probs.argsort()])
    print(accuracy, recall, precision, AP)

    # graph the decision surface
    decfun = binlogreg_decfun(w, b)
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset=0.5)

    # graph the data points
    data.graph_data(X, Y_, Y, special=[])

    # show the plot
    plt.show()
