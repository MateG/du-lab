import matplotlib.pyplot as plt
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim

from lab1 import data
from lab1.util import one_hot_encode, graph_result


class PTDeep(nn.Module):
    def __init__(self, config, transfer_function):
        super().__init__()

        self.config = config
        self.weights: nn.ParameterList = nn.ParameterList()
        self.biases: nn.ParameterList = nn.ParameterList()
        for i in range(len(config) - 1):
            tensor = torch.randn((config[i], config[i + 1]))
            self.weights.append(nn.Parameter(tensor, requires_grad=True))
            tensor = torch.zeros(config[i + 1])
            self.biases.append(nn.Parameter(tensor, requires_grad=True))

        self.transfer_function = transfer_function

    def forward(self, X):
        self.weights: nn.ParameterList
        self.biases: nn.ParameterList

        weights = list(self.weights.parameters())
        biases = list(self.biases.parameters())

        h = X
        last_layer = len(weights) - 1
        for i in range(0, last_layer):
            h = self.transfer_function(h.mm(weights[i]) + biases[i])
        h = h.mm(weights[last_layer]) + biases[last_layer]
        return torch.softmax(h, dim=1)

    def get_loss(self, X, Yoh_, param_lambda):
        self.weights: nn.ParameterList
        self.biases: nn.ParameterList

        loss = -torch.log((self.forward(X).clamp_min(1e-6) * Yoh_).sum(dim=1)).mean()
        if param_lambda != 0.0:
            for W in self.weights.parameters():
                loss += param_lambda * (W.norm() ** 2)
        return loss


def train(model, X, Yoh_, param_niter, param_delta, param_lambda=0.0):
    optimizer = optim.SGD(model.parameters(), lr=param_delta)

    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_, param_lambda)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        if i % 100 == 0:
            print("iteration {}: loss {}".format(i, loss))


def eval(model, X):
    return model.forward(torch.tensor(X).float()).detach().numpy()


def count_params(model):
    total_count = 0
    for name, param in model.named_parameters():
        print(name, param.size())
        count = 1
        for i in param.size():
            count *= i
        total_count += count
    print("Total parameters:", total_count)


def deep_decfun(model):
    if model.config[-1] == 2:
        def classify(X):
            return eval(model, X)[:, 0]
    else:
        def classify(X):
            return np.argmax(eval(model, X), axis=1)

    return classify


def train_and_graph(X, Y_, config, transfer_function, param_niter=10_000, param_delta=0.1, param_lambda=0.0):
    ptd: PTDeep = PTDeep(config, transfer_function)
    train(ptd, torch.tensor(X).float(), torch.tensor(one_hot_encode(Y_)), param_niter, param_delta, param_lambda)

    Y = np.argmax(eval(ptd, X), axis=1)
    graph_result(deep_decfun(ptd), X, Y_, Y)


def multiple_train_and_graph(title, datasets, configs, transfer_function, params_niter, params_delta):
    plt.figure(figsize=(10, 6))
    plt.suptitle(title)
    plt.subplots_adjust(hspace=0.3)

    rows = len(datasets)
    cols = len(configs)
    current_sub = 1

    for X, Y_ in datasets:
        for config, param_niter, param_delta in zip(configs, params_niter, params_delta):
            print("Training {}-{}".format(title, config))

            plt.subplot(rows, cols, current_sub)
            plt.title(config)
            train_and_graph(X, Y_, config, transfer_function, param_niter, param_delta)

            current_sub += 1


if __name__ == '__main__':
    np.random.seed(100)
    torch.random.manual_seed(100)

    X, Y_ = data.sample_gauss_2d(3, 100)

    ptd: PTDeep = PTDeep([2, 3], torch.sigmoid)
    train(ptd, torch.tensor(X).float(), torch.tensor(one_hot_encode(Y_)), 1000, 0.5)

    print("count_params:")
    count_params(ptd)

    Y = np.argmax(eval(ptd, X), axis=1)
    graph_result(deep_decfun(ptd), X, Y_, Y)
    plt.show()

    np.random.seed(100)
    torch.random.manual_seed(100)
    X1, Y1_ = data.sample_gmm_2d(4, 2, 40)

    np.random.seed(100)
    torch.random.manual_seed(100)
    X2, Y2_ = data.sample_gmm_2d(6, 2, 10)

    train_and_graph(X2, Y2_, [2, 10, 10, 2], torch.relu, 10_000, 0.05, 1e-4)
    plt.show()

    datasets = [(X1, Y1_), (X2, Y2_)]
    configs = [[2, 2], [2, 10, 2], [2, 10, 10, 2]]
    params_niter = [2_000, 5_000, 10_000]
    params_delta = [0.5, 0.1, 0.01]

    multiple_train_and_graph("ReLU", datasets, configs, torch.relu, params_niter, params_delta)
    multiple_train_and_graph("Sigmoid", datasets, configs, torch.sigmoid, params_niter, params_delta)
    plt.show()
