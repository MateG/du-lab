import math
import os

import matplotlib.pyplot as plt
import numpy as np
import skimage as ski
import skimage.io
import torch
from torch import nn
from torch import optim


def shuffle_data(data_x, data_y):
    indices = np.arange(data_x.shape[0])
    np.random.shuffle(indices)

    shuffled_data_x = np.ascontiguousarray(data_x[indices])
    shuffled_data_y = np.ascontiguousarray(data_y[indices])

    return shuffled_data_x, shuffled_data_y


def draw_image(img, mean, std):
    img = img.transpose(1, 2, 0)
    img *= std
    img += mean
    img = img.astype(np.uint8)
    ski.io.imshow(img)
    # ski.io.show()


def draw_conv_filters(epoch, step, weights, save_dir):
    w = weights.copy()

    num_filters = w.shape[0]
    num_channels = w.shape[1]
    k = w.shape[2]
    assert w.shape[3] == w.shape[2]

    w = w.transpose(2, 3, 1, 0)
    w -= w.min()
    w /= w.max()

    border = 1
    cols = 8
    rows = math.ceil(num_filters / cols)
    width = cols * k + (cols - 1) * border
    height = rows * k + (rows - 1) * border

    img = np.zeros([height, width, num_channels])

    for i in range(num_filters):
        r = int(i / cols) * (k + border)
        c = int(i % cols) * (k + border)

        img[r:r + k, c:c + k, :] = w[:, :, :, i]

    filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
    ski.io.imsave(os.path.join(save_dir, filename), img)


def plot_training_progress(save_dir, data):
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2, 2, figsize=(16, 8))

    linewidth = 2
    legend_size = 10
    train_color = 'm'
    val_color = 'c'

    num_points = len(data['train_loss'])
    x_data = np.linspace(1, num_points, num_points)

    ax1.set_title('Cross-entropy loss')
    ax1.plot(x_data, data['train_loss'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='train')
    ax1.plot(x_data, data['valid_loss'], marker='o', color=val_color,
             linewidth=linewidth, linestyle='-', label='validation')
    ax1.legend(loc='upper right', fontsize=legend_size)

    ax2.set_title('Average class accuracy')
    ax2.plot(x_data, data['train_acc'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='train')
    ax2.plot(x_data, data['valid_acc'], marker='o', color=val_color,
             linewidth=linewidth, linestyle='-', label='validation')
    ax2.legend(loc='upper left', fontsize=legend_size)

    ax3.set_title('Learning rate')
    ax3.plot(x_data, data['lr'], marker='o', color=train_color,
             linewidth=linewidth, linestyle='-', label='learning_rate')
    ax3.legend(loc='upper left', fontsize=legend_size)

    save_path = os.path.join(save_dir, 'training_plot.png')
    print('Plotting in: ', save_path)
    plt.savefig(save_path)


def train(train_x, train_y, valid_x, valid_y, model, config):
    max_epochs = config['max_epochs']
    batch_size = config['batch_size']
    save_dir = config['save_dir']
    weight_decay = config['weight_decay']
    starting_lr = config['starting_lr']
    gamma = config['gamma']

    num_examples = train_x.shape[0]
    assert num_examples % batch_size == 0
    num_batches = num_examples // batch_size

    plot_data = {}
    plot_data['train_loss'] = []
    plot_data['valid_loss'] = []
    plot_data['train_acc'] = []
    plot_data['valid_acc'] = []
    plot_data['lr'] = []

    optimizer = optim.SGD(model.parameters(), starting_lr, weight_decay=weight_decay)
    lr_scheduler = optim.lr_scheduler.ExponentialLR(optimizer, gamma)

    for epoch in range(1, max_epochs + 1):
        X, Y = shuffle_data(train_x, train_y)
        X = torch.FloatTensor(X)
        Y = torch.LongTensor(Y)

        for batch in range(num_batches):
            batch_X = X[batch * batch_size:(batch + 1) * batch_size, :]
            batch_Y = Y[batch * batch_size:(batch + 1) * batch_size]

            loss = model.get_loss(batch_X, batch_Y)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

            if batch % 100 == 0:
                print("epoch: {}, step: {}/{}, batch_loss: {}".format(epoch, batch, num_batches, loss))

            if batch % 200 == 0:
                draw_conv_filters(epoch, batch, model.conv1.weight.detach().cpu().numpy(), save_dir)

        train_loss, train_acc = evaluate("Train", model, train_x, train_y)
        val_loss, val_acc = evaluate("Validation", model, valid_x, valid_y)

        plot_data['train_loss'] += [train_loss]
        plot_data['valid_loss'] += [val_loss]
        plot_data['train_acc'] += [train_acc]
        plot_data['valid_acc'] += [val_acc]
        plot_data['lr'] += [lr_scheduler.get_lr()]

        lr_scheduler.step()

    plot_training_progress(save_dir, plot_data)


def evaluate(name, model, x, yt, return_yp=False):
    print("\nRunning evaluation:", name)

    yp = None
    loss = 0.0
    with torch.no_grad():
        # calculate in slices to avoid hitting swap when low on memory
        slice_size = 5000
        for start in range(0, x.shape[0] - slice_size + 1, slice_size):
            end = min(start + slice_size, x.shape[0])
            slice_tensor = torch.FloatTensor(x[start:end, ])
            yp_slice = model.forward(slice_tensor)
            yp = yp_slice if yp is None else torch.cat((yp, yp_slice))

        criterion = nn.CrossEntropyLoss()
        loss = criterion(yp, torch.LongTensor(yt)).item()

    print(" Loss:", loss)

    yp = yp.detach().numpy()
    C = yp.shape[1]

    confusion = np.empty((C, C), dtype=int)
    for ct in range(C):
        ct_indices = np.argwhere(yt == ct).flatten()
        for cp in range(C):
            confusion[ct][cp] = (yp[ct_indices].argmax(axis=1) == cp).sum()

    print(" Confusion matrix:")
    print(confusion)

    tp = np.diag(confusion)
    fp = np.sum(confusion, axis=0) - tp
    fn = np.sum(confusion, axis=1) - tp

    accuracy = 1.0 * tp.sum() / yp.shape[0]
    print(" Accuracy:", accuracy)

    precisions = tp / (tp + fp)
    recalls = tp / (tp + fn)

    print(" Precisions:")
    print(precisions)

    print(" Recalls:")
    print(recalls)

    if return_yp:
        return yp
    return loss, accuracy
