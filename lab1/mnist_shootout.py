import matplotlib.pyplot as plt
import torch
import torch.optim as optim
import torchvision

from lab1.data import eval_perf_multi
from lab1.pt_deep import PTDeep, train as ptd_train


def main():
    dataset_root = '/tmp/mnist'  # change this to your preference
    mnist_train = torchvision.datasets.MNIST(dataset_root, train=True, download=True)
    mnist_test = torchvision.datasets.MNIST(dataset_root, train=False, download=True)

    x_train, y_train = mnist_train.data, mnist_train.targets
    x_test, y_test = mnist_test.data, mnist_test.targets
    x_train, x_test = x_train.float().div_(255.0), x_test.float().div_(255.0)

    N = x_train.shape[0]  # 60_000
    D = x_train.shape[1] * x_train.shape[2]  # 784
    C = y_train.max().add_(1).item()  # 10

    sub_task_1(x_train, y_train, N, D, C)
    # sub_task_2(x_train, y_train, x_test, y_test, N, D, C)
    # sub_task_3(x_train, y_train, x_test, y_test, N, D, C)


def sub_task_1(x_train, y_train, N, D, C):
    config = [D, C]
    param_niter = 201
    params_delta = [1.0, 1.5, 2.0]
    params_lambda = [0.1, 0.01, 0.001]

    plt.figure(figsize=(14, 5))
    plt.subplots_adjust(wspace=0.6, hspace=0.3)

    rows = len(params_lambda)
    cols = C
    for row, (param_lambda, param_delta) in enumerate(zip(params_lambda, params_delta)):
        print("Training with lambda={}".format(param_lambda))

        ptd: PTDeep = PTDeep(config=config, transfer_function=torch.relu)
        ptd_train(
            ptd,
            x_train.reshape(N, -1),
            torch.eye(C)[y_train],
            param_niter, param_delta, param_lambda
        )

        W = next(ptd.parameters()).detach().numpy()
        for c in range(C):
            plt.subplot(rows, cols, row * cols + c + 1)
            plt.title(str(c))
            plt.imshow(
                W[:, c].reshape(x_train.shape[1], -1),
                cmap=plt.get_cmap('gray')
            )

    plt.show()


def sub_task_2(x_train, y_train, x_test, y_test, N, D, C):
    configs = [[D, C], [D, 100, C]]
    params_niter = [100, 1_000]
    params_delta = [0.7, 0.3]

    plt.figure(figsize=(14, 8), dpi=80)
    plt.subplots_adjust(wspace=0.3, hspace=0.5)
    rows = len(configs) * 2
    cols = 4

    ptds = []

    for i, (config, param_niter, param_delta) in enumerate(zip(configs, params_niter, params_delta)):
        print("Training config {}".format(config))

        ptd: PTDeep = PTDeep(config=config, transfer_function=torch.relu)
        ptds.append(ptd)
        train_stats, test_stats = train(
            ptd,
            x_train.reshape(N, -1),
            y_train,
            torch.eye(C)[y_train],
            x_test.reshape(x_test.shape[0], -1),
            y_test,
            param_niter, param_delta
        )

        base_index = i * 8

        plt.subplot(rows, cols, base_index + 1)
        plt.title("Loss " + str(config))
        plt.plot(train_stats['loss'])

        plt.subplot(rows, cols, base_index + 2)
        plt.title("Train accuracy")
        plt.plot(train_stats['accuracy'])

        plt.subplot(rows, cols, base_index + 3)
        plt.title("Train precision (by class)")
        plt.plot(train_stats['precision'])

        plt.subplot(rows, cols, base_index + 4)
        plt.title("Train recall (by class)")
        plt.plot(train_stats['recall'])

        plt.subplot(rows, cols, base_index + 6)
        plt.title("Test accuracy")
        plt.plot(train_stats['accuracy'])

        plt.subplot(rows, cols, base_index + 7)
        plt.title("Test precision (by class)")
        plt.plot(train_stats['precision'])

        plt.subplot(rows, cols, base_index + 8)
        plt.title("Test recall (by class)")
        plt.plot(train_stats['recall'])

    plt.show()


def sub_task_3(x_train, y_train, x_test, y_test, N, D, C):
    config = [D, 100, C]
    param_niter = 1_000
    param_delta = 0.3
    params_lambda = [0.1, 0.01, 0.001]

    plt.figure(figsize=(14, 8), dpi=80)
    plt.subplots_adjust(wspace=0.3, hspace=0.8)
    rows = len(params_lambda) * 2
    cols = 4

    ptds = []

    for i, param_lambda in enumerate(params_lambda):
        print("Training with lambda={}".format(param_lambda))

        ptd: PTDeep = PTDeep(config=config, transfer_function=torch.relu)
        ptds.append(ptd)
        train_stats, test_stats = train(
            ptd,
            x_train.reshape(N, -1),
            y_train,
            torch.eye(C)[y_train],
            x_test.reshape(x_test.shape[0], -1),
            y_test,
            param_niter, param_delta, param_lambda
        )

        base_index = i * 8

        plt.subplot(rows, cols, base_index + 1)
        plt.title("Loss lambda=" + str(param_lambda))
        plt.plot(train_stats['loss'])

        plt.subplot(rows, cols, base_index + 2)
        plt.title("Train accuracy")
        plt.plot(train_stats['accuracy'])

        plt.subplot(rows, cols, base_index + 3)
        plt.title("Train precision (by class)")
        plt.plot(train_stats['precision'])

        plt.subplot(rows, cols, base_index + 4)
        plt.title("Train recall (by class)")
        plt.plot(train_stats['recall'])

        plt.subplot(rows, cols, base_index + 6)
        plt.title("Test accuracy")
        plt.plot(train_stats['accuracy'])

        plt.subplot(rows, cols, base_index + 7)
        plt.title("Test precision (by class)")
        plt.plot(train_stats['precision'])

        plt.subplot(rows, cols, base_index + 8)
        plt.title("Test recall (by class)")
        plt.plot(train_stats['recall'])

    plt.show()


def train(model, X, Y, Yoh_, X_test, Y_test, param_niter, param_delta, param_lambda=0.0):
    train_stats = {'loss': [], 'accuracy': [], 'recall': [], 'precision': []}
    test_stats = {'accuracy': [], 'recall': [], 'precision': []}

    optimizer = optim.SGD(model.parameters(), lr=param_delta)

    for i in range(param_niter):
        loss = model.get_loss(X, Yoh_, param_lambda)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        train_stats['loss'].append(loss.detach())
        print("iteration {}: loss {}".format(i, loss))

        a, rp, _ = eval_perf_multi(model.forward(X).detach().numpy().argmax(axis=1), Y)
        train_stats['accuracy'].append(a)
        train_stats['recall'].append([x[0] for x in rp])
        train_stats['precision'].append([x[1] for x in rp])

        a, rp, _ = eval_perf_multi(model.forward(X_test).detach().numpy().argmax(axis=1), Y_test)
        test_stats['accuracy'].append(a)
        test_stats['recall'].append([x[0] for x in rp])
        test_stats['precision'].append([x[1] for x in rp])

    return train_stats, test_stats


if __name__ == '__main__':
    main()
