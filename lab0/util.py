import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def step(x):
    return np.heaviside(x - 0.5, 1)


def stable_softmax(x):
    exp_x_shifted = np.exp(x - np.max(x))
    return exp_x_shifted / np.sum(exp_x_shifted)
