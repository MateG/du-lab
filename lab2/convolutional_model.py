import torch
from torch import nn


class ConvolutionalModel(nn.Module):
    def __init__(self, input_layer,
                 conv1_width, conv2_width,
                 fc1_width, class_count,
                 conv_kernel=5, pool_kernel=2, pool_stride=2):
        super().__init__()

        N, C, H, W = input_layer.shape

        self.conv1 = nn.Conv2d(C, conv1_width,
                               kernel_size=conv_kernel,
                               padding=(conv_kernel - 1) // 2,
                               bias=True)
        self.pool1 = nn.MaxPool2d(pool_kernel, pool_stride)
        H //= pool_stride
        W //= pool_stride

        self.conv2 = nn.Conv2d(conv1_width, conv2_width,
                               kernel_size=conv_kernel,
                               padding=(conv_kernel - 1) // 2,
                               bias=True)
        self.pool2 = nn.MaxPool2d(pool_kernel, pool_stride)
        H //= pool_stride
        W //= pool_stride

        self.fc1 = nn.Linear(conv2_width * H * W, fc1_width, bias=True)
        self.fc_logits = nn.Linear(fc1_width, class_count, bias=True)

        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) \
                    or (isinstance(m, nn.Linear) and m is not self.fc_logits):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc_logits.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = self.pool1(h)
        h = torch.relu(h)

        h = self.conv2(h)
        h = self.pool2(h)
        h = torch.relu(h)

        h = h.view(h.shape[0], -1)
        h = self.fc1(h)
        h = torch.relu(h)

        logits = self.fc_logits(h)

        return logits
