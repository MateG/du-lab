from operator import itemgetter

import torch
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset


class NLPDataset(Dataset):

    @classmethod
    def from_file(cls, path):
        def to_instance(line):
            parts = line.split(', ')
            return parts[0].split(), parts[1]

        with open(path, 'r') as f:
            return cls(list(map(to_instance, f.read().splitlines())))

    def __init__(self, instances):
        self.instances = instances

        self.text_vocab = None
        self.label_vocab = None

    def __getitem__(self, i):
        instance = self.instances[i]
        return self.text_vocab.encode(instance[0]), self.label_vocab.encode(instance[1])

    def __len__(self):
        return len(self.instances)

    def set_vocabs(self, text_vocab, label_vocab):
        self.text_vocab = text_vocab
        self.label_vocab = label_vocab

    def text_frequencies(self):
        counts = dict()
        for text in map(lambda i: i[0], self.instances):
            for symbol in text:
                counts[symbol] = counts.get(symbol, 0) + 1

        return counts

    def label_frequencies(self):
        counts = dict()
        for label in map(lambda i: i[1], self.instances):
            counts[label] = counts.get(label, 0) + 1

        return counts


class Vocab:

    def __init__(self, frequencies, max_size, min_freq, special_symbols=True):
        items = filter(lambda i: i[1] >= min_freq, frequencies.items())
        items = sorted(items, key=itemgetter(1), reverse=True)
        symbols = list(map(lambda s: s[0], items))

        if special_symbols:
            symbols.insert(0, '<PAD>')
            symbols.insert(1, '<UNK>')

        if max_size > 0:
            symbols = symbols[:max_size]

        self.itos = {i: symbol for i, symbol in enumerate(symbols)}
        self.stoi = {symbol: i for i, symbol in enumerate(symbols)}

    def encode(self, t):
        def encode_token(token):
            i = self.stoi.get(token)
            return i if i is not None else 1

        return torch.tensor(encode_token(t) if isinstance(t, str) else list(map(encode_token, t)))


def pad_collate_fn(batch, pad_index=0):
    texts, labels = zip(*batch)
    lengths = torch.tensor([len(text) for text in texts])
    texts = pad_sequence(texts, batch_first=True, padding_value=pad_index)
    labels = torch.tensor(labels)
    return texts, labels, lengths


def embedding_matrix(vocab, d, path=None):
    matrix = torch.randn((len(vocab), d))
    row_range = torch.arange(d)
    matrix[0, row_range] = 0

    if path is not None:
        with open(path, 'r') as f:
            for parts in map(lambda l: l.split(), f.read().splitlines()):
                i = vocab.get(parts[0])
                if i is not None:
                    matrix[i, row_range] = torch.tensor(list(map(lambda s: float(s), parts[1:d + 1])))

    return matrix
