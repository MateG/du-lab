from common import common_main, load_datasets, Args
from torch import nn


class RNNModel(nn.Module):

    def __init__(self, embedding, fc_transfer, rnn, hidden_size, num_layers, dropout, bidirectional):
        super().__init__()

        self.embedding = embedding

        self.fc_transfer = fc_transfer

        self.rnn = rnn(300, hidden_size, num_layers=num_layers, dropout=dropout, bidirectional=bidirectional)
        self.fc1 = nn.Linear(hidden_size, hidden_size)
        self.fc2 = nn.Linear(hidden_size, 1)

    def forward(self, x):
        out, _ = self.rnn(self.embedding(x[0].T))

        if self.rnn.bidirectional:
            first_out = out[0]
            last_out = out[-1]
            h = first_out[:, self.rnn.hidden_size:] + last_out[:, :self.rnn.hidden_size]
        else:
            h = out[-1]

        h = self.fc1(h)
        h = self.fc_transfer(h)

        h = self.fc2(h)
        return h


def main(datasets, args, rnn, hidden_size=150, num_layers=2, dropout=0.0, bidirectional=False):
    return common_main(
        datasets, args,
        lambda embedding: RNNModel(embedding, args.fc_transfer, rnn, hidden_size, num_layers, dropout, bidirectional)
    )


if __name__ == '__main__':
    datasets = load_datasets(
        'dataset/sst_train_raw.csv',
        'dataset/sst_valid_raw.csv',
        'dataset/sst_test_raw.csv'
    )

    main(datasets, Args(), nn.RNN)
