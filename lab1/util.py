import numpy as np

from lab1 import data


def one_hot_encode(Y_):
    N, C = Y_.shape[0], np.max(Y_) + 1
    Yoh_ = np.zeros((N, C))
    Yoh_[np.arange(N), Y_] = 1
    return Yoh_


def graph_result(decfun, X, Y_, Y, special=[], offset=0.5):
    bbox = (np.min(X, axis=0), np.max(X, axis=0))
    data.graph_surface(decfun, bbox, offset)
    data.graph_data(X, Y_, Y, special)
