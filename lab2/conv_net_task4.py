import torch
from torch import nn


class ConvNet(nn.Module):
    def __init__(self, C=3, H=32, W=32):
        super().__init__()

        self.conv1 = nn.Conv2d(C, 16, 5, 1, 2, bias=True)
        self.pool1 = nn.MaxPool2d(3, 2, 1)
        H //= 2
        W //= 2

        self.conv2 = nn.Conv2d(16, 32, 5, 1, 2, bias=True)
        self.pool2 = nn.MaxPool2d(3, 2, 1)
        H //= 2
        W //= 2

        self.fc1 = nn.Linear(32 * H * W, 256, bias=True)
        self.fc2 = nn.Linear(256, 128, bias=True)
        self.fc_logits = nn.Linear(128, 10, bias=True)

        self.reset_parameters()

        self.criterion = nn.CrossEntropyLoss()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d) \
                    or (isinstance(m, nn.Linear) and m is not self.fc_logits):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc_logits.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = torch.relu(h)
        h = self.pool1(h)

        h = self.conv2(h)
        h = torch.relu(h)
        h = self.pool2(h)

        h = h.view(h.shape[0], -1)

        h = self.fc1(h)
        h = torch.relu(h)

        h = self.fc2(h)
        h = torch.relu(h)

        logits = self.fc_logits(h)

        return logits

    def get_loss(self, x, y):
        logits = self.forward(x)
        return self.criterion(logits, y)
