import os
import pickle
from pathlib import Path

import matplotlib.pyplot as plt
import nn_task4
import numpy as np
from conv_net_task4 import ConvNet


def unpickle(file):
    fo = open(file, 'rb')
    dict = pickle.load(fo, encoding='latin1')
    fo.close()
    return dict


DATA_DIR = Path(__file__).parent / 'datasets' / 'cifar-10-batches-py'
SAVE_DIR = Path(__file__).parent / 'out'

config = {}
config['max_epochs'] = 20
config['batch_size'] = 50
config['save_dir'] = SAVE_DIR
config['weight_decay'] = 1e-3
config['starting_lr'] = 0.05
config['gamma'] = 0.85

img_height = 32
img_width = 32
num_channels = 3
num_classes = 10

train_x = np.ndarray((0, img_height * img_width * num_channels), dtype=np.float32)
train_y = []

for i in range(1, 6):
    subset = unpickle(os.path.join(DATA_DIR, 'data_batch_%d' % i))
    train_x = np.vstack((train_x, subset['data']))
    train_y += subset['labels']

train_x = train_x.reshape((-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1)
train_y = np.array(train_y, dtype=np.int32)

subset = unpickle(os.path.join(DATA_DIR, 'test_batch'))
test_x = subset['data'].reshape((-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1).astype(np.float32)
test_y = np.array(subset['labels'], dtype=np.int32)

valid_size = 5000
train_x, train_y = nn_task4.shuffle_data(train_x, train_y)
valid_x = train_x[:valid_size, ...]
valid_y = train_y[:valid_size, ...]
train_x = train_x[valid_size:, ...]
train_y = train_y[valid_size:, ...]
data_mean = train_x.mean((0, 1, 2))
data_std = train_x.std((0, 1, 2))

train_x = (train_x - data_mean) / data_std
valid_x = (valid_x - data_mean) / data_std
test_x = (test_x - data_mean) / data_std

train_x = train_x.transpose(0, 3, 1, 2)
valid_x = valid_x.transpose(0, 3, 1, 2)
test_x = test_x.transpose(0, 3, 1, 2)

net = ConvNet()
nn_task4.train(train_x, train_y, valid_x, valid_y, net, config)
yp = nn_task4.evaluate("Test", net, test_x, test_y, True)

yp_of_true = yp[np.arange(yp.shape[0]), test_y]
worst_indices = yp_of_true.argsort()[:20]

plt.close()
plt.figure(figsize=(16, 8), dpi=60)

for i, data_index in enumerate(worst_indices, 1):
    plt.subplot(5, 5, i)
    t = test_y[data_index]
    p = yp[data_index].argsort()[::-1][:3]
    plt.title("T: " + str(t) + ", P: " + str(p))
    nn_task4.draw_image(test_x[data_index], data_mean, data_std)

plt.show()
